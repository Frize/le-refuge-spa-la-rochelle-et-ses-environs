<?php get_header();

while (have_posts()) :
    the_post();

    $especes = get_the_terms(get_the_ID(), 'espece');
    $ages = get_the_terms(get_the_ID(), 'tranche_age');
    $sexes = get_the_terms(get_the_ID(), 'sexe');

    $iden = get_field('identite');
    $sante = get_field('carne')

?>
    <main class="un-animal">
        <h1><?= the_title() ?></h1>
        <section class="swiper animal-swiper">
            <ul class="swiper-wrapper">
                <?php

                $photos = get_field('photos'); // Récupérer les photos depuis le champ personnalisé

                if ($photos) : // Vérifier si le champ contient des photos
                    foreach ($photos as $photo) :
                        if (!empty($photo)) : // Vérifier si la photo existe
                ?>
                            <li class="swiper-slide"><img src="<?= $photo ?>" alt=""></li>
                <?php
                        endif;
                    endforeach;
                endif;
                ?>
            </ul>
        </section>

        <section class="description">
            <h2>Qui suis-je</h2>
            <p><?= the_field('who') ?></p>
            <div class="description__buttons">
                <button onclick="openModal('modale-adoption')" class="description__button button">Information adoption</button>
                <a href="" class="description__button button button--secondary">Contactez le refuge</a>
            </div>
        </section>

        <section class="identite">
            <div class="identite__card">
                <h3>Carte d'identité</h3>
                <?php foreach ($sexes as $sexe) : ?>
                    <p>Sexe : <?= $sexe->name; ?></p>
                <?php endforeach; ?>
                <p>Date de naissance : <?= $iden['date_de_naissance']; ?> </p>
            </div>
            <div class="identite__card identite__card--light">
                <h3>Test</h3>
                <p><?= $sante['vaccine']; ?></p>
                <p><?= $sante['puce']; ?></p>
                <p><?= $sante['sterilise']; ?></p>
            </div>
            <div class="identite__card">
                <h3>Personnalitée</h3>
                <?= the_field('personnalite') ?>
            </div>
            <div class="identite__card identite__card--light">
                <h3>S'entend avec</h3>
                <?php $ententes = get_field('entente');
                 foreach ($ententes as $entente) : ?>
                    <p><?php echo $entente; ?></p>
                <?php endforeach; ?>
            </div>
        </section>

        <div class="modal" id="modale-adoption">
            <div class="modal__header">
                <button onclick="closeModal()" class="modal__close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-x">
                        <path d="M18 6 6 18" />
                        <path d="m6 6 12 12" />
                    </svg></button>
            </div>
            <div class="modal__container">
                <div class="modal__col">
                    <h2>Information sur l'adoption</h2>
                    <div>
                        <figure><img src="<?= get_template_directory_uri() ?>/assets/img/picto/info-color.svg" alt="">
                            <figcaption>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, magni quibusdam voluptatem id deleniti, dolorem fugit voluptate possimus soluta dolore molestiae animi? Nihil nobis eius eveniet rerum consectetur exercitationem voluptatibus.</p>
                            </figcaption>
                        </figure>
                        <figure><img src="<?= get_template_directory_uri() ?>/assets/img/picto/info-color.svg" alt="">
                            <figcaption>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </figcaption>
                        </figure>
                        <figure><img src="<?= get_template_directory_uri() ?>/assets/img/picto/validation-color.svg" alt="">
                            <figcaption>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="modal__col">
                    <h2>Frais d’adoption - {Espèce}</h2>
                    <div>
                        <figure><img src="<?= get_template_directory_uri() ?>/assets/img/picto/venus-color.svg" alt="">
                            <figcaption>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </figcaption>
                        </figure>
                        <figure><img src="<?= get_template_directory_uri() ?>/assets/img/picto/mars-color.svg" alt="">
                            <figcaption>
                                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
        <div onclick="closeModal()" class="modal__overlay"></div>

    </main>

<?php endwhile; ?>

<?php get_footer() ?>