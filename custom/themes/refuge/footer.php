        <footer class="footer">
            <div class="footer__head">
                <img class="footer__logo" alt="Logo" src="<?=get_template_directory_uri()?>/assets/img/logo/logo.svg">
            </div>
            <div class="footer__coordonnees coordonees">
                <h2 class="coordonees__lieu">Où nous retrouver</h2>
                <p class="coordonees__adresse">Rue de la Guignarderie - 17140 LAGORD</p>
                <h2 class="coordonees__days">Horaire d’ouverture</h2>
                <div class="coordonees__datetime">
                    <img alt="Calendrier" src="<?=get_template_directory_uri()?>/assets/img/icons/calendrier.svg">
                    <div class="coordonees__details">
                        <p class="coordonees__date">Lun au Ven</p>
                        <p class="coordonees__time">13h à 17h</p>
                    </div>
                </div>
                <div>
                    <a class="coordonees__fb" href="#">Retrouvez nous aussi sur</a>
                </div>
            </div>
            <div class="footer__map">
                <h2><a href="#">Villes partenaires</a></h2>
                <img src="<?=get_template_directory_uri()?>/assets/img/img/carte.png" alt="Image">
            </div>
            <div class="footer__mention">
                <p>Copyright © 2024 Refuge SPA de La Rochelle et ses Environs. Tous droits réservés</p>
                <ul>
                    <li><a href="#">Mentions légales</a></li>
                    <li><a href="#">Politique de confidentialité</a></li>
                </ul>
                <ul>
                    <li><a href="#"><img src="<?=get_template_directory_uri()?>/assets/img/logo/france-relance.png" alt="Logo partenaire"></a></li>
                    <li><a href="#"><img src="<?=get_template_directory_uri()?>/assets/img/logo/lr-univ.png" alt="Logo partenaire"></a></li>
                    <li><a href="#"><img src="<?=get_template_directory_uri()?>/assets/img/logo/pawweb.png" alt="Logo partenaire"></a></li>
                </ul>
            </div>
        </footer>
        <?php wp_footer() ?>
    </body>
</html>