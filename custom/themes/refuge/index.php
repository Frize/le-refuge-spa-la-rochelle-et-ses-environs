<?php 
get_header(); 
while (have_posts()) : 
    the_post(); 
?>
<main class="single">
    <section class="single__header" style="background-image: url('<?php echo has_post_thumbnail() ? the_post_thumbnail_url() : '' ?>');">
        <div class="opacite"></div>
        <h2><?php echo the_title() ?></h2>
    </section>
    <section class="gutenberg-content">
        <?php echo the_content() ?>
    </section>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>
