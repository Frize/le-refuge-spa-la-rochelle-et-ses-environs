<?php

function temoignageCPT()
{
    $labels = array(
        'name' => 'Temoignages',
        'singular_name' => 'Temoignage',
        'menu_name' => 'Temoignages',
        'add_new' => 'Ajouter un temoignage',
        'add_new_item' => 'Ajouter un temoignage',
        'edit_item' => 'Modifier un temoignage',
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array(
            'title',
            'thumbnail',
            'custom-fields',
        ),
        'menu_icon' => 'dashicons-feedback'
    );

    register_post_type('temoignages', $args);
}


add_action('init', 'temoignageCPT');