<?php

function petAlertCPT()
{
    $labels = array(
        'name' => 'Pet Alerts',
        'singular_name' => 'Pet Alert',
        'menu_name' => 'Pet Alerts',
        'add_new' => 'Ajouter une section',
        'add_new_item' => 'Ajouter une section',
        'edit_item' => 'Modifier une section',
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array(
            'title',
            'custom-fields',
        ),
        'menu_icon' => 'dashicons-warning'
    );

    register_post_type('petAlert', $args);
}


add_action('init', 'petAlertCPT');