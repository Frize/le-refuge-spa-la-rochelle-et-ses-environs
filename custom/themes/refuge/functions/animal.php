<?php

// code Lamia : ajout d'un custom post type pour modifier la section de telechargement d'un fichier depuis la médiathéque

function animal_cpt()
{
    $labels = array(
        'name' => 'Animaux',
        'menu_name' => 'Animaux',
        'singular_name' => 'Animal',
        'add_new' => 'Ajouter un animal',
        'add_new_item' => 'Ajouter un animal',
        'edit_item' => 'Modifier la fiche d\'un animal',
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array(
            'title',
            'thumbnail',
            'custom-fields',
        ),
        'menu_icon' => 'dashicons-pets',
    );
    register_post_type('animaux', $args);

    // taxonomie 'espece'
    $labels_espece = array(
        'name' => 'Espece',
        'menu_name' => 'Espèce',
        'add_new_item' => 'Ajouter une espèce',
        'edit_item' => 'Modifier une espèce',

    );
    $args_espece = array(
        'labels' => $labels_espece,
        'public' => true,
        'hierarchical' => true,
        'show_admin_column' => true

    );
    register_taxonomy('espece', 'animaux',  $args_espece);

    //taxonomie sexe
    $labels_sexe = array(
        'name' => 'Sexe',
        'menu_name' => 'Sexe',
    );
    $args_sexe = array(
        'labels' => $labels_sexe,
        'public' => true,
        'hierarchical' => true,
        'show_admin_column' => true
    );
    register_taxonomy('sexe', 'animaux',  $args_sexe);

    //taxonomie age
    $labels_age = array(
        'name' => 'Tranche d\'âge',
        'menu_name' => 'Tranche d\'âge',
        'add_new_item' => 'Ajouter une tranche d\'âge',
        'edit_item' => 'Modifier une tranche d\'âge',

    );
    $args_age = array(
        'labels' => $labels_age,
        'public' => true,
        'show_admin_column' => true,
        'hierarchical' => true,

    );
    register_taxonomy('tranche_age', 'animaux',  $args_age);
}
add_action('init', 'animal_cpt');