<?php

/* ==========================================================================
    APPELS JS & CSS
========================================================================== */
function custom_enqueue_scripts() {
    wp_enqueue_style( 'css-main', get_template_directory_uri() . '/assets/css/main.min.css', null, '1.0.0', 'all' );
    wp_enqueue_script( 'js-swiper', get_template_directory_uri() . '/assets/vendors/swiper-bundle.min.js', null, '1.0.0', true);
    wp_enqueue_script( 'js-main', get_template_directory_uri() . '/assets/js/scripts.min.js', null, '1.0.0', true);

}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' );
