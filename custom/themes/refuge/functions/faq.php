<?php

function faqCPT()
{
    $labels = array(
        'name' => 'FAQ',
        'singular_name' => 'Pet Alert',
        'menu_name' => 'FAQ',
        'add_new' => 'Ajouter une question',
        'add_new_item' => 'Ajouter une question',
        'edit_item' => 'Modifier une question',
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
        ),
        'menu_icon' => 'dashicons-format-status'
    );

    register_post_type('faq', $args);
}


add_action('init', 'faqCPT');