<?php

/* ==========================================================================
    DECLARATION DES MENUS
========================================================================== */

add_action('after_setup_theme', 'registrer_menus');
function registrer_menus()
{
    register_nav_menus(array(
        'main-menu' => 'Menu principal',
    ));
}
