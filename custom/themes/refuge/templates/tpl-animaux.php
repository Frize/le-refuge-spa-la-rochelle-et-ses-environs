<?php
/*
  Template Name: Animaux
*/

get_header();

?>

<main class="nos-animaux">
    <h1><?= the_title() ?></h1>
    <aside class="filter">
        <div class="filter__head">
            <h2>Filtre</h2>
            <button class="filter__button">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-circle-chevron-down">
                    <circle cx="12" cy="12" r="10" />
                    <path d="m16 10-4 4-4-4" />
                </svg>
            </button>
        </div>

        <form class="filter__body">
            <h3>Espèces</h3>
            <ul class="filter__row">
                <li class="filter__checkbox">
                    <input type="checkbox" name="espece" id="espece1">
                    <label for="espece1"><span>Chien</span></label>
                </li>
                <li class="filter__checkbox">
                    <input type="checkbox" name="espece" id="espece2">
                    <label for="espece2"><span>Chat</span></label>
                </li>
                <li class="filter__checkbox">
                    <input type="checkbox" name="espece" id="espece3">
                    <label for="espece3"><span>Autres</span></label>
                </li>
            </ul>
            <h3>Sexe</h3>
            <ul class="filter__row">
                <li class="filter__checkbox">
                    <input type="checkbox" name="sexe" id="sexe1">
                    <label for="sexe1"><span>Mâle</span></label>
                </li>
                <li class="filter__checkbox">
                    <input type="checkbox" name="sexe" id="sexe2">
                    <label for="sexe2"><span>Femelle</span></label>
                </li>
            </ul>
            <h3>Âge</h3>
            <ul class="filter__row">
                <li class="filter__checkbox">
                    <input type="checkbox" name="age" id="age1">
                    <label for="age1"><span>Jeune</span></label>
                </li>
                <li class="filter__checkbox">
                    <input type="checkbox" name="age" id="age2">
                    <label for="age2"><span>Adulte</span></label>
                </li>
                <li class="filter__checkbox">
                    <input type="checkbox" name="age" id="age3">
                    <label for="age3"><span>Senior</span></label>
                </li>
            </ul>

            <button class="button filter__submit" type="submit">Rechercher <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-search">
                    <circle cx="11" cy="11" r="8" />
                    <path d="m21 21-4.3-4.3" />
                </svg></button>
        </form>
    </aside>
    <!--teste Lamia -->

    <section class="list-animaux">
        <h2 class="is-hidden">Liste de nos animaux</h2>
        <?php
        $args = array(
            'post_type' => 'animaux',
            'posts_per_page' => -1
        );

        $query = new WP_Query($args);

        if ($query->have_posts()) :
            while ($query->have_posts()) :
                $query->the_post();

                $especes = get_the_terms(get_the_ID(), 'espece');
                $ages = get_the_terms(get_the_ID(), 'tranche_age');
                $sexes = get_the_terms(get_the_ID(), 'sexe');


                $animal = get_field('identite');
                if ($animal) :
                    $race = $animal['race'];
        ?>

                    <div class="card-animal">
                        <a class="card-animal__img" href="<?php the_permalink() ?>"><img src="<?= the_post_thumbnail_url() ?>" alt=""></a>
                        <div class="card-animal__head">
                            <a href="<?php the_permalink() ?>">
                                <h3><?= get_the_title(); ?></h3>
                            </a>
                            <div>
                                <?php foreach ($sexes as $sexe) : ?>
                                    <?php if ($sexe->name === "Femelle") : ?>
                                        <!-- Femelle-->
                                        <svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                            <path d="M80 176a112 112 0 1 1 224 0A112 112 0 1 1 80 176zM224 349.1c81.9-15 144-86.8 144-173.1C368 78.8 289.2 0 192 0S16 78.8 16 176c0 86.3 62.1 158.1 144 173.1V384H128c-17.7 0-32 14.3-32 32s14.3 32 32 32h32v32c0 17.7 14.3 32 32 32s32-14.3 32-32V448h32c17.7 0 32-14.3 32-32s-14.3-32-32-32H224V349.1z" />
                                        </svg>
                                        <a class="card-animal__fav" href=""><svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path d="M225.8 468.2l-2.5-2.3L48.1 303.2C17.4 274.7 0 234.7 0 192.8v-3.3c0-70.4 50-130.8 119.2-144C158.6 37.9 198.9 47 231 69.6c9 6.4 17.4 13.8 25 22.3c4.2-4.8 8.7-9.2 13.5-13.3c3.7-3.2 7.5-6.2 11.5-9c0 0 0 0 0 0C313.1 47 353.4 37.9 392.8 45.4C462 58.6 512 119.1 512 189.5v3.3c0 41.9-17.4 81.9-48.1 110.4L288.7 465.9l-2.5 2.3c-8.2 7.6-19 11.9-30.2 11.9s-22-4.2-30.2-11.9zM239.1 145c-.4-.3-.7-.7-1-1.1l-17.8-20c0 0-.1-.1-.1-.1c0 0 0 0 0 0c-23.1-25.9-58-37.7-92-31.2C81.6 101.5 48 142.1 48 189.5v3.3c0 28.5 11.9 55.8 32.8 75.2L256 430.7 431.2 268c20.9-19.4 32.8-46.7 32.8-75.2v-3.3c0-47.3-33.6-88-80.1-96.9c-34-6.5-69 5.4-92 31.2c0 0 0 0-.1 .1s0 0-.1 .1l-17.8 20c-.3 .4-.7 .7-1 1.1c-4.5 4.5-10.6 7-16.9 7s-12.4-2.5-16.9-7z" />
                                            </svg></a>
                                    <?php else : ?>
                                        <!-- Male-->
                                        <svg height="24" width="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                            <path d="M289.8 46.8c3.7-9 12.5-14.8 22.2-14.8H424c13.3 0 24 10.7 24 24V168c0 9.7-5.8 18.5-14.8 22.2s-19.3 1.7-26.2-5.2l-33.4-33.4L321 204.2c19.5 28.4 31 62.7 31 99.8c0 97.2-78.8 176-176 176S0 401.2 0 304s78.8-176 176-176c37 0 71.4 11.4 99.8 31l52.6-52.6L295 73c-6.9-6.9-8.9-17.2-5.2-26.2zM400 80l0 0h0v0zM176 416a112 112 0 1 0 0-224 112 112 0 1 0 0 224z" />
                                        </svg>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <ul class="card-animal__body">
                            <li>
                                <p>Race</p>
                                <p><?= $race; ?></p>
                            </li>
                            <?php if ($ages) : ?>
                                <li>
                                    <p>Age</p>
                                    <?php foreach ($ages as $age) : ?>
                                        <p class="age"><?= $age->name; ?></p>
                                    <?php endforeach; ?>
                                </li>
                            <?php endif;
                            if ($sexes) : ?>
                                <li>
                                    <p>Genre</p>
                                    <?php foreach ($sexes as $sexe) : ?>
                                        <p class="genre"><?= $sexe->name; ?></p>
                                    <?php endforeach; ?>
                                </li>
                            <?php endif;
                            if ($especes) : ?>
                                <li>
                                    <p>Espèces</p>
                                    <?php foreach ($especes as $espece) : ?>
                                        <p class="espece"><?= $espece->name; ?></p>
                                    <?php endforeach; ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
        <?php
                endif;
            endwhile;
            wp_reset_postdata(); // Réinitialisez la requête
        endif;
        ?>
    </section>

    <!--fin teste Lamia -->
</main>

<script defer>
    document.addEventListener("DOMContentLoaded", () => {
        // Obtenez toutes les checkboxes pour les différents critères
        const checkboxes = {
            sexe: document.querySelectorAll('input[name="sexe"]'),
            age: document.querySelectorAll('input[name="age"]'),
            espece: document.querySelectorAll('input[name="espece"]')
        };

        // Obtenez toutes les cartes des animaux
        const animalCards = document.querySelectorAll(".card-animal");

        // Fonction pour filtrer les animaux selon les critères sélectionnés
        const filterAnimals = () => {
            // Obtenir les critères sélectionnés
            const selectedCriteria = {
                sexe: Array.from(checkboxes.sexe)
                    .filter(checkbox => checkbox.checked)
                    .map(checkbox => checkbox.labels[0].textContent.trim()),
                age: Array.from(checkboxes.age)
                    .filter(checkbox => checkbox.checked)
                    .map(checkbox => checkbox.labels[0].textContent.trim()),
                espece: Array.from(checkboxes.espece)
                    .filter(checkbox => checkbox.checked)
                    .map(checkbox => checkbox.labels[0].textContent.trim())
            };

            // Parcourez les cartes des animaux pour afficher ou masquer selon les critères sélectionnés
            animalCards.forEach(card => {
                const cardSexe = card.querySelector('.genre').textContent.trim();
                const cardAge = card.querySelector('.age').textContent.trim();
                const cardEspece = card.querySelector('.espece').textContent.trim();

                // Pour chaque critère, vérifiez s'il est sélectionné, et si oui, s'il correspond à la carte
                const matchesSexe = selectedCriteria.sexe.length === 0 || selectedCriteria.sexe.includes(cardSexe);
                const matchesAge = selectedCriteria.age.length === 0 || selectedCriteria.age.includes(cardAge);
                const matchesEspece = selectedCriteria.espece.length === 0 || selectedCriteria.espece.includes(cardEspece);

                // Afficher la carte si elle correspond à tous les critères sélectionnés
                if (matchesSexe && matchesAge && matchesEspece) {
                    card.style.display = "";
                } else {
                    card.style.display = "none";
                }
            });
        };

        // Appliquez l'événement 'change' pour chaque groupe de checkboxes
        Object.values(checkboxes).forEach(group => {
            group.forEach(checkbox => {
                checkbox.addEventListener("change", filterAnimals);
            });
        });

        // Appliquez le filtrage au chargement initial
        filterAnimals();
    });
</script>

<?php get_footer();
