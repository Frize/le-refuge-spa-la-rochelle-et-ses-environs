// Initialize modules
const { src, dest, watch, series, parallel } = require('gulp')
const sass = require('gulp-sass')(require('sass')) // sass compiler
const sourcemaps = require('gulp-sourcemaps') // css sourcemaps
const autoprefixer = require('gulp-autoprefixer') // autoprefix css
const imagemin = require('gulp-imagemin') // image optimization
const cleancss = require('gulp-clean-css') // minify css
const concat = require('gulp-concat') // concat files
const rename = require('gulp-rename') // rename files
const uglify = require('gulp-uglify') // minify js

// File paths
const paths = {
  imgPath: 'img/**/*',
  cssPath: 'scss/**/*.scss',
  jsPath: 'js/**/*.js',
  fontsPath: 'fonts/**/*',
  vendorsPath: 'vendors/**/*',
}

// Images
function imgTask() {
  return src(paths.imgPath).pipe(imagemin()).pipe(dest('../assets/img'))
}

// Scss
function cssTask() {
  return src(paths.cssPath)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cleancss())
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(sourcemaps.write('./'))
    .pipe(dest('../assets/css'))
}

// Javascript
function jsTask() {
  return src(paths.jsPath)
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(dest('../assets/js'))
}

// Vendors
function vendorsTask() {
  return src(paths.vendorsPath).pipe(dest('../assets/vendors'))
}

// Fonts
function fontsTask() {
  return src(paths.fontsPath).pipe(dest('../assets/fonts'))
}

// Watch
function watchTask() {
  watch(paths.cssPath, cssTask)
  watch(paths.jsPath, jsTask)
  watch(paths.imgPath, imgTask)
  watch(paths.fontsPath, fontsTask)
}

// Complex tasks
const building = parallel(imgTask, cssTask, jsTask, vendorsTask, fontsTask)
const watching = parallel(watchTask)
const rebuilding = parallel(imgTask, cssTask, jsTask, vendorsTask, fontsTask)

// Exports tasks
exports.img = imgTask
exports.css = cssTask
exports.js = jsTask
exports.vendors = vendorsTask
exports.fonts = fontsTask
exports.prod = building
exports.default = building
exports.watch = watching
exports.rebuild = rebuilding