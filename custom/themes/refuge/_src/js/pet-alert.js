const petTitleBtn = document.getElementById('pet-title');
const alertList = document.querySelector('.p-alert__list');

if(petTitleBtn){
  let isMobileView = false;

  function toggleDisplay() {
    alertList.style.display = isMobileView ? (alertList.style.display === 'none' ? 'flex' : 'none') : 'flex';
  }

  function checkScreenSize() {
    const currentIsMobile = window.innerWidth <= 756;
    if (currentIsMobile !== isMobileView) {
      isMobileView = currentIsMobile;
      alertList.style.display = isMobileView ? 'none' : 'flex';
    }
  }

  petTitleBtn.addEventListener('click', toggleDisplay);
  window.addEventListener('resize', checkScreenSize);

  checkScreenSize();
}