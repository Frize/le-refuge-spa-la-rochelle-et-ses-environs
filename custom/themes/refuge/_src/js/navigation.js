document.addEventListener("DOMContentLoaded", function () {
    var toggleButtonMenu = document.getElementById("toggle-menu");
    var navigation = document.querySelector(".navigation");
    var closeNavigation = document.getElementById("close-icon");
    var openNavigation = document.getElementById("open-icon");
    var isMobile = false; // Variable pour suivre si l'écran est en mode mobile

    function toggleMenu() {
        var expanded =
            toggleButtonMenu.getAttribute("aria-expanded") === "true" || false;
        toggleButtonMenu.setAttribute("aria-expanded", !expanded);

        if (!expanded) {
            hideIcon(openNavigation);
            expandNavigation();
            showIcon(closeNavigation);
        } else {
            hideIcon(closeNavigation);
            collapseNavigation();
            showIcon(openNavigation);
        }
    }

    function hideIcon(icon) {
        icon.style.display = "none";
    }

    function showIcon(icon) {
        icon.style.display = "block";
    }

    function expandNavigation() {
        navigation.classList.add("expanded-navigation");
    }

    function collapseNavigation() {
        navigation.classList.remove("expanded-navigation");
    }

    function handleResize() {
        var currentIsMobile = window.innerWidth <= 1024; // Taille de la tablette

        if (currentIsMobile !== isMobile) {
            isMobile = currentIsMobile; // Mettre à jour l'état de l'écran mobile

            if (isMobile) {
                showIcon(openNavigation);
                hideIcon(closeNavigation);
            } else {
                hideIcon(openNavigation);
                hideIcon(closeNavigation);
            }

            // Si le menu est actuellement étendu, le réduire
            if (toggleButtonMenu.getAttribute("aria-expanded") === "true") {
                collapseNavigation();
                toggleButtonMenu.setAttribute("aria-expanded", "false");
            }
        }
    }

    toggleButtonMenu.addEventListener("click", toggleMenu);
    window.addEventListener("resize", handleResize);

    // Initialisation lors du chargement de la page
    handleResize();
});

document.addEventListener("DOMContentLoaded", function () {
    const toggleSubButton = document.getElementById("btn-sub-menu");
    const submenu = document.querySelector(".sub-menu");
    let isSubMenuOpen = false;

    function toggleMenu() {
        const expanded = toggleSubButton.getAttribute("aria-expanded") === "true";
        toggleSubButton.setAttribute("aria-expanded", String(!expanded));
        submenu.classList.toggle("expanded-sub-navigation");
        isSubMenuOpen = !isSubMenuOpen;
        if (isSubMenuOpen) {
            const { bottom, left } = toggleSubButton.getBoundingClientRect();
            submenu.style.top = `${bottom + 30}px`;
            submenu.style.left = `${left}px`;
            document.addEventListener('click', closeMenuOutside);
        } else {
            document.removeEventListener('click', closeMenuOutside);
        }
    }

    function closeMenuOutside(event) {
        if (!submenu.contains(event.target) && event.target !== toggleSubButton) {
            toggleMenu();
        }
    }

    toggleSubButton.addEventListener("click", function(event) {
        event.stopPropagation();
        toggleMenu();
    });
});
