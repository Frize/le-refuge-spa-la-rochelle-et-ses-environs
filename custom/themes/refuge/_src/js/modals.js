var OpenModal = '';

function openModal(idModal){
    console.log(idModal);
    document.querySelector('.modal__overlay').style.display = 'block';
    document.querySelector(`#${idModal}`).style.display = 'block';
    OpenModal = idModal;
}

function closeModal(){
    document.querySelector('.modal__overlay').style.display = 'none';
    document.querySelector(`#${OpenModal}`).style.display = 'none';
    OpenModal = '';
}

