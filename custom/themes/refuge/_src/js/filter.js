var filterButton = document.querySelector('.filter__button');
var filterBody = document.querySelector('.filter__body');
var filterIsOpen = false;

if(filterButton){
    filterButton.addEventListener('click', function() {
        filterIsOpen = !filterIsOpen;
        if (filterIsOpen) {
            filterBody.style.display = 'flex';
            this.style.transform = 'rotate(180deg)';
        } else {
            filterBody.style.display = 'none';
            this.style.transform = 'rotate(0deg)';
        }
    });
}