var swiper = new Swiper(".homeSwipe", {
    loop: true,
    autoplay: {
        delay: 25000,
        disableOnInteraction: false,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

var swiper = new Swiper(".animal-swiper", {
    slidesPerView: "auto",
    spaceBetween: 15,
    loop: true,
});
