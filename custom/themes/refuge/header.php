<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
    <title><?= the_title()?></title>
</head>

<body>
    <header>
        <nav role="navigation" aria-label="menu-principal" class="navigation">
            <div class="navigation__head">
                <!-- TODO : Mettre le lien sur toutes les pages sauf front-page -->
                <a href="/">
                    <img class="navigation__logo" alt="Logo avec texte" src="<?=get_template_directory_uri()?>/assets/img/logo/logo.svg">
                </a>
                <button aria-expanded="true" id="toggle-menu" class="navigation__toggle" aria-controls="menu-principal">
                    <img class="navigation__open navigation__picto navigation__open" alt="Menu buger" id="open-icon" src="<?=get_template_directory_uri()?>/assets/img/icons/burger.svg">
                    <img class="navigation__close navigation__picto navigation__close" alt="Close Burger" id="close-icon" src="<?=get_template_directory_uri()?>/assets/img/icons/x.svg">
                </button>
            </div>
            <ul class="navigation__list">
                <li>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main_menu',
                        // 'container' => null, // Cela désactive le conteneur autour du menu
                    ));
                    ?>
                </li>
                <li>
                    <ul class="navigation__links">
                        <li class="navigation__facebook"><a href="#"><span class="hide--tab hide--desk">Notre page Facebook</span><span class="hide--mobile">
                                    <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M25 13.0466C25 6.11655 19.4043 0.5 12.5 0.5C5.5957 0.5 0 6.11655 0 13.0466C0 18.9278 4.03809 23.868 9.48242 25.2255V16.8791H6.9043V13.0466H9.48242V11.3949C9.48242 7.12615 11.4063 5.14615 15.5859 5.14615C16.377 5.14615 17.7441 5.30298 18.3057 5.45981V8.92972C18.0127 8.90031 17.5 8.88071 16.8604 8.88071C14.8096 8.88071 14.0186 9.65997 14.0186 11.6841V13.0466H18.1006L17.3975 16.8791H14.0137V25.5C20.2051 24.7501 25 19.462 25 13.0466Z" fill="#3B5998" />
                                    </svg>
                                </span></a></li>
                                TODO : FAV
                        <li class="navigation__favori"><a href="#"><span class="hide--tab hide--desk">Vos coups de cœur</span> <span class="hide--mobile">
                                    <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.09433 0.00599098C6.04743 0.00960922 5.89548 0.0258961 5.75667 0.0421829C2.97092 0.353432 0.684169 2.3856 0.12702 5.04388C-0.122478 6.2382 0.00133294 7.77092 0.447803 9.06659C0.920536 10.4292 1.71405 11.6199 3.06847 12.997C3.71003 13.6485 4.2578 14.137 6.04743 15.6571C6.48077 16.0244 6.96289 16.4352 7.11671 16.5709C9.03766 18.2358 10.3714 19.6328 11.5176 21.18L11.7615 21.5111L12 22L12.2567 21.5093L12.5006 21.1619C13.5455 19.6744 14.7742 18.3389 16.5995 16.7085C16.9578 16.3864 17.3649 16.0335 18.5261 15.0382C20.3195 13.4965 21.2368 12.6079 21.9797 11.6905C23.1859 10.1976 23.7955 8.78972 23.9756 7.07061C24.0113 6.71593 24.0075 5.86724 23.9663 5.55961C23.7693 4.10289 23.139 2.85066 22.0941 1.84272C21.0492 0.834782 19.7492 0.224949 18.2409 0.0367527C17.8789 -0.00667763 17.091 -0.00667763 16.7214 0.0367527C15.3464 0.205044 14.1345 0.737064 13.1197 1.61833C12.7614 1.92958 12.358 2.37293 12.116 2.72218C12.0598 2.80361 12.0091 2.87056 12.0035 2.87056C11.9979 2.87056 11.9472 2.80361 11.8909 2.72218C11.6489 2.37293 11.2456 1.92958 10.8873 1.61833C9.8912 0.753349 8.69249 0.219522 7.36433 0.0494213C7.09045 0.0132294 6.31006 -0.012104 6.09433 0.00599098Z" fill="#212121" />
                                    </svg>
                                </span></a></li>
                        <li class="search__container">
                            <form action="#" class="search">
                                <input class="search__input" type="text" placeholder="Rechercher...">
                                <button class="" type="submit">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="search__submit feather feather-search" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                        <circle cx="11" cy="11" r="8" />
                                        <path d="M21 21l-4.35-4.35" />
                                    </svg>
                                </button>
                            </form>
                        </li>
                        <li><a href="https://preprod.refuge.frize.xyz/donnation-refuge/" class="button button--arround">Nous soutenir</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>