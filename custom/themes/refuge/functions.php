<?php

add_theme_support( 'post-thumbnails' );

/**
 * Refuge SPA La Rochelle et ses Environs
 */
require 'functions/scripts.php';
require 'functions/navigation.php';
require 'functions/animal.php';
require 'functions/fil-ariane.php';
require 'functions/pet-alerts.php';
require 'functions/faq.php';
require 'functions/temoignages.php';