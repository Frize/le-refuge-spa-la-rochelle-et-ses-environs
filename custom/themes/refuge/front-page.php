<?php get_header() ?>

<?php
$bienvenue = get_field('bienvenue');
$bulleM =  get_field('moyenne_bulle');
$bulleS =  get_field('petite_bulle');
$actions = [
    1 => get_field('action_1'),
    2 => get_field('action_2'),
    3 => get_field('action_3'),
];

$haveAction = function (array $actions) {
    $total = 0;
    foreach ($actions as $action) {
        if (!empty($action['nom_action'])) {
            $total++;
        }
    }
    if ($total > 0) {
        return true;
    };
    return false;
};

$args = array(
    'post_type' => 'petAlert',
);
$pet_alert_query = new WP_Query($args);

$args = array(
    'post_type' => 'faq',
);
$faq_query = new WP_Query($args);

$args = array(
    'posts_per_page' => 3,
    'post_type' => 'post',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC'
);
$recent_posts_query = new WP_Query($args);

$args = array(
    'posts_per_page' => 10,
    'post_type' => 'temoignages',
    'orderby' => 'date',
    'order' => 'DESC'
);
$temoignage_query = new WP_Query($args);

$howToHelp = get_field('how_to_help');
?>

<main class="front-page">
    <section class="head">
        <div class="head__welcome">
            <h1 class="head__welcome--title">Bienvenue au refuge</h1>
            <p class="head__welcome--subtitle"><?= $bienvenue['welcome'] ?></p>
            <a class="head__welcome--btn button" href="<?= $bienvenue['page_en_savoir_plus'] ?>">En savoir plus <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-home">
                    <path d="m3 9 9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z" />
                    <polyline points="9 22 9 12 15 12 15 22" />
                </svg></a>
        </div>

        <div class="blob blob--large">
            <img class="blob__img" src="<?= get_template_directory_uri() ?>/assets/img/photo/photo_chien.png" alt="Chien et photo-benevole">
            <p class="blob__text">“ Je m’appelle Ali, croisé berger allemand de bientôt 8 ans transféré au refuge depuis un autre refuge [...] ”.</p>
            <a href="" class="button button--secondary">Adoptez-moi</a>
        </div>
        <div class="blob">
            <!-- TODO : Demander à lamia l'ext -->
            <img class="blob__img" src="<?= $bulleM['image_bulle_m'] ?>" alt="">
            <p class="blob__text"><?= $bulleM['texte_m'] ?></p>
            <a href="<?= $bulleM['lien_du_bouton_m'] ?>" class="button button--secondary"><?= $bulleM['libelle_bouton_m'] ?></a>
        </div>

        <div class="blob blob--little">
            <p class="blob__text"><?= $bulleS['text'] ?></p>
            <a href="<?= $bulleS['link'] ?>" class="button button--secondary"><?= $bulleS['libelle_bouton'] ?></a>
        </div>
    </section>
    <?php if ($pet_alert_query->have_posts()) : ?>
        <section class="p-alert">
            <h2>Une urgence animale ?</h2>
            <button id="pet-title" class="p-alert__title">Une urgence animale ?</button>
            <ul class="p-alert__list">
                <?php while ($pet_alert_query->have_posts()) : $pet_alert_query->the_post();
                    $description = get_field('description');
                ?>
                    <li>
                        <!-- TODO : Faire un choix sur l'emplacement ! Ici quand l'écrans reduit la modal se ferme mais pas l'overlay -->
                        <button onclick="openModal('modale-withimg')"><img src="<?php the_field('icone'); ?>" alt="icon de <?= get_the_title() ?>"><?= get_the_title() ?></button>
                        <div class="modal" id="modale-withimg">
                            <div class="modal__container">
                                <div class="modal__header">
                                    <h3><?= get_the_title() ?></h3>
                                    <button onclick="closeModal()" class="modal__close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-x">
                                            <path d="M18 6 6 18" />
                                            <path d="m6 6 12 12" />
                                        </svg></button>
                                </div>
                                <?php if ($description['image'] || $description['text']) : ?>
                                    <div class="modal__body p-alert__content">
                                        <?php if ($description['image']) : ?>
                                            <figure>
                                                <img src="<?= $description['image'] ?>" alt="Icon A CHANGER">
                                                <figcaption><?= $description['text'] ?></figcaption>
                                            </figure>
                                        <?php else : ?>
                                            <?= $description['text'] ?>
                                        <?php endif; ?>

                                    </div>
                                <?php endif;
                                $contact = get_field('contact');
                                if ($contact['telephone'] || $contact['mail'] || $contact['adresse'] || $contact['web_site']) :
                                ?>
                                    <div class="modal__footer">
                                        <?php if ($contact['adresse']) : ?>
                                            <figure>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-map-pin">
                                                    <path d="M20 10c0 6-8 12-8 12s-8-6-8-12a8 8 0 0 1 16 0Z" />
                                                    <circle cx="12" cy="10" r="3" />
                                                </svg>
                                                <figcaption>
                                                    <p><?= $contact['adresse']; ?></p>
                                                </figcaption>
                                            </figure>
                                        <?php endif;
                                        if ($contact['telephone']) : ?>
                                            <figure>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-phone">
                                                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z" />
                                                </svg>
                                                <figcaption>
                                                    <a href="tel:<?= $contact['telephone'] ?>;"><?= $contact['telephone'] ?></a>
                                                </figcaption>
                                            </figure>
                                        <?php endif;
                                        if ($contact['mail']) : ?>
                                            <figure>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-mail">
                                                    <rect width="20" height="16" x="2" y="4" rx="2" />
                                                    <path d="m22 7-8.97 5.7a1.94 1.94 0 0 1-2.06 0L2 7" />
                                                </svg>
                                                <figcaption>
                                                    <a href="mailto:<?= $contact['mail'] ?>"><?= $contact['mail'] ?></a>
                                                </figcaption>
                                            </figure>
                                        <?php endif;
                                        if ($contact['web_site']) : ?>
                                            <figure>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-monitor">
                                                    <rect width="20" height="14" x="2" y="3" rx="2" />
                                                    <line x1="8" x2="16" y1="21" y2="21" />
                                                    <line x1="12" x2="12" y1="17" y2="21" />
                                                </svg>
                                                <figcaption>
                                                    <a href="<?= $contact['web_site'] ?>"><?= $contact['web_site'] ?></a>
                                                </figcaption>
                                            </figure>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </ul>
        </section>
    <?php endif; ?>

    <?php if ($haveAction) : ?>
        <section class="action">
            <h2>Nos actions</h2>
            <ul>
                <?php foreach ($actions as $action) :
                    if ($action['nom_action']) :
                ?>
                        <li class="card card--service">
                            <h3 class="card__title"><?= $action['nom_action'] ?></h3>
                            <img class="card__thumbnail" src="<?= $action['image'] ?>" alt="<?= $action['nom_action'] ?>">
                            <p class="card__subtitle"><?= $action['pour'] ?></p>
                            <div>
                                <p class="card__text"><?= $action['description'] ?></p>
                            </div>
                            <?php if ($action['link']) : ?>
                                <a href="<?= $action['link'] ?>" class="button button--fullwidth card__button"><?= $action['libelle_bouton'] ?> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke-width="3.5" stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M5 12h14" />
                                        <path d="M12 5v14" />
                                    </svg></a>
                            <?php endif; ?>
                        </li>
                <?php
                    endif;
                endforeach; ?>
            </ul>
        </section>
    <?php endif; ?>
    <?php if ($faq_query->have_posts()) : ?>
        <section class="faq">
            <h2>Foire aux questions</h2>
            <div class="faq__content">
                <!-- TODO : Ajouter image dans ACF Front page -->
                <img class="faq__img" src="<?= get_template_directory_uri() ?>/assets/img/photo/action-pension.png" alt="image animal">
                <ul class="faq__list">
                    <?php while ($faq_query->have_posts()) : $faq_query->the_post(); ?>
                        <li>
                            <details class="faq__item">
                                <summary class="faq__question"><?= the_title() ?></summary>
                                <?= the_content() ?>
                            </details>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </section>
    <?php wp_reset_postdata();
    endif; ?>

    <?php if ($faq_query->have_posts()) : ?>
        <section class="actualites">
            <h2>Nos actualités</h2>
            <ul>
                <?php while ($recent_posts_query->have_posts()) : $recent_posts_query->the_post(); ?>
                    <li class="card">
                        <h3 class="card__title"><?= the_title() ?></h3>
                        <img class="card__thumbnail" src="<?= the_post_thumbnail_url(); ?>" alt="<?= the_title() ?>">
                        <div class="card__text">
                            <p><?= the_excerpt() ?></p>
                        </div>
                        <a href="<?= the_permalink() ?>" class="button button--fullwidth card__button">Lire l'article</a>
                    </li>
                <?php endwhile; ?>
            </ul>
        </section>
    <?php wp_reset_postdata();
    endif; ?>

    <?php if ($howToHelp) : ?>
        <section class="dons">
            <h2>Vous souhaitez nous aider ?</h2>
            <ul>
                <?php if ($howToHelp['section_1']['titre'] || $howToHelp['section_1']['description']) : ?>
                    <li>
                        <img src="<?= get_template_directory_uri() ?>/assets/img/picto/dons_heart.svg" alt="Coeur dons">
                        <h3><?= $howToHelp['section_1']['titre'] ?></h3>
                        <p><?= $howToHelp['section_1']['description'] ?></p>
                    </li>
                <?php endif; ?>
                <?php if ($howToHelp['section_2']['titre'] || $howToHelp['section_2']['description']) : ?>
                    <li>
                        <img src="<?= get_template_directory_uri() ?>/assets/img/picto/dons_heart.svg" alt="Coeur dons">
                        <h3><?= $howToHelp['section_2']['titre'] ?></h3>
                        <p><?= $howToHelp['section_2']['description'] ?></p>
                    </li>
                <?php endif; ?>
                <?php if ($howToHelp['section_3']['titre'] || $howToHelp['section_3']['description']) : ?>
                    <li>
                        <img src="<?= get_template_directory_uri() ?>/assets/img/picto/dons_heart.svg" alt="Coeur dons">
                        <h3><?= $howToHelp['section_3']['titre'] ?></h3>
                        <p><?= $howToHelp['section_3']['description'] ?></p>
                    </li>
                <?php endif; ?>
            </ul>
            <a href="<?= $howToHelp['page_dons'] ?>" class="button">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-heart">
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                </svg>
                Nous faire un don
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-heart">
                    <path d="M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z" />
                </svg>
            </a>
        </section>
    <?php endif; ?>

    <?php if ($temoignage_query->have_posts()) : ?>
        <section class="temoins">
            <h2>Ils nous ont fait leurs retours !</h2>
            <div class="swiper homeSwipe">
                <ul class="swiper-wrapper">
                    <?php while ($temoignage_query->have_posts()) : $temoignage_query->the_post(); ?>
                        <li class="swiper-slide temoins__item">
                            <figure>
                                <img src="<?= the_field('image') ?>" alt="<?= the_title() ?>">
                                <figcaption>
                                    <h3><?= the_title() ?></h3>
                                    <p><?= the_field('temoignage') ?></p>
                                </figcaption>
                            </figure>
                        </li>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </ul>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
    <?php endif; ?>

</main>

<div onclick="closeModal()" class="modal__overlay"></div>
<?php get_footer() ?>