<?php get_header() ?>

<main class="actualites">
    <h2><?php the_title() ?></h2>
    <?php if (have_posts()) : ?>
        <ul>
            <?php while (have_posts()) : the_post(); ?>
                <li class="card">
                    <h3 class="card__title"><?php the_title(); ?></h3>
                    <?php if (has_post_thumbnail()) : ?>
                        <img class="card__thumbnail" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title_attribute(); ?>">
                    <?php endif; ?>
                    <div class="card__text">
                        <p><?php the_excerpt() ?></p>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="button button--fullwidth card__button">
                        En savoir
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke-width="3.5" stroke-linecap="round" stroke-linejoin="round">
                            <path d="M5 12h14" />
                            <path d="M12 5v14" />
                        </svg>
                    </a>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php else : ?>
        <p>Aucun article trouvé.</p>
    <?php endif; ?>

    <div>
        <?php
        echo paginate_links(array(
            'prev_text' => '<a class="button"><span class="page-link">' . __('Previous', 'textdomain') . '</span></a>',
            'next_text' => '<a class="button"><span class="page-link">' . __('Next', 'textdomain') . '</span></a>',
            'before_page_number' => '<li class="page-item"><span class="page-link">',
            'after_page_number' => '</span></li>',
        ));
        ?>

    </div>
</main>

<?php get_footer() ?>