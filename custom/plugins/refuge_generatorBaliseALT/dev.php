<?php

/**
 * Plugin Name: Generateur description image
 * Description: Ce plugin genere automatique des balises ALT utile à l'accessiblité
 * Vesrion : 1.0
 * Author: Pawweb
 * Author URI: /
 */

function update_image_alt_tags_on_upload($post_ID)
{
    if (wp_attachment_is_image($post_ID)) {
        //solution 1 => déscription de la balise ALT à partir du nom de l'image
        $image_name = get_post($post_ID)->post_title;
        $image_name = preg_replace('%\s*[-_\s]+\s*%', ' ', $image_name); // Replace '-''_' with single space
        $image_name = ucwords(strtolower($image_name)); // Capitalize first letter*/
        $image_url = wp_get_attachment_url($post_ID);

        //solution 2 => description de la balise ALT à partir de chatGPT
        if ($image_url) {
            $image_description = $image_url;
            if ($image_description !== false) {
                // Récupérer la balise alt existante
                $existing_alt = get_post_meta($post_ID, '_wp_attachment_image_alt', true);
                if (empty($existing_alt)) {
                    //solution 1
                    update_post_meta($post_ID, '_wp_attachment_image_alt', $image_name);
                    // Récupérer le nom du fichier sans extension
                    $image_name = pathinfo(get_attached_file($post_ID), PATHINFO_FILENAME);
                    // Remplacer les caractères spéciaux par des espaces dans le nom du fichier
                    $image_name = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $image_name);
                    // Mettre à jour le titre de l'image avec le nom du fichier et que la legende soit vide
                    wp_update_post(array(
                        'ID' => $post_ID,
                        'post_title' => $image_name,
                        'post_excerpt' => ''
                    ));
                }
            }
        }
    }
}
add_action('add_attachment', 'update_image_alt_tags_on_upload');
