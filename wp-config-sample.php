<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link https://fr.wordpress.org/support/article/editing-wp-config-php/ Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'refuge' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'z6;c#@eScMu;VrGNg8D(xHYvG&#6u4[gLZY*iOap[{4$FlD ZC|&=#vW&aH*>P)X' );
define( 'SECURE_AUTH_KEY',  'ni#Anib$z&]7,M+a;9@d-]&E5J@eXFh1O:v-Db,b.W+QylJq!H}d+[/-t29Kc=;&' );
define( 'LOGGED_IN_KEY',    '~8+IK?BjhhfPHp+aeVqw?(WI]yTf><(/oTo^Wl1IS?^&zhIVm,Sz:w`r=%>~Ps2c' );
define( 'NONCE_KEY',        'd/g]s3sYpu9cwHjWyxn0A>})^vS21%#?^C1fdPQb*@Y|fR[ISFL~[/dPYr1#zF3m' );
define( 'AUTH_SALT',        'Y^ty [fN)wc8Zk,c,m6H^0m``c)7Sqs2]7fgp&9T$!x4lxuTLzHQ54Ol/4n5%Enh' );
define( 'SECURE_AUTH_SALT', 'r=RSI9l@;su%.a+m$*|;6t3Lw5G%cCGhhEa G%?EwlJ8_a~v,@W/dmx}i(mXl(}L' );
define( 'LOGGED_IN_SALT',   'YgwE7Uhn$G2KW$y-q7CAC26gA{#IjwZqkifEqNoE.iil!mJ]W<l+72QqQ)4sA4]g' );
define( 'NONCE_SALT',       '_Bmb+F7_}h/8aSl|ev#EtJ=ZC(QB(:nr7_0SlOW:y[rV!yOCJ(@sK;tFGs87Se`I' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'rlg_';

/**
 * Pour les développeurs et développeuses : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs et développeuses d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur la documentation.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

define( 'DISALLOW_FILE_EDIT', true );
define( 'WP_CONTENT_FOLDERNAME', 'custom' );
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/' . WP_CONTENT_FOLDERNAME );
define( 'WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/' );
define( 'WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
